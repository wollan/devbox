#!/usr/bin/env bash
# shellcheck disable=SC2155,SC2207
set -eu -o pipefail

BRANCH="$(git symbolic-ref --short -q HEAD)"

function readsymlink {
    if hash greadlink 2>/dev/null; then
        greadlink -f "$@"
    else
        readlink -f "$@"
    fi
}

function upload_readme {
    local pw="$1"
    local token="$(jq -n '{ username: "wollan", password: $pw }' --arg pw "$pw" \
        | curl -sSL -X POST https://hub.docker.com/v2/users/login \
        --data-binary @- \
        -H 'Content-Type: application/json' \
        | jq -r .token)"
    local code="$(jq -n '{ full_description: $readme }' --arg readme "$(cat README.md)" \
        | curl -sSL -X PATCH "https://hub.docker.com/v2/repositories/wollan/devbox/" \
        --data-binary @- \
        -H 'Content-Type: application/json' \
        -H "Authorization: JWT $token" \
        -o /dev/null -w "%{http_code}")"
    if [[ "$code" != 200 ]]; then
        echo "failed to upload readme with code $code"
        exit 42
    fi
    echo "readme uploaded"
}

function build {
    local image="${1?'missing image'}"

    if podman manifest exists "$image"; then
        podman manifest rm "$image"
    fi

    podman manifest create "$image"

    # build
    podman build \
        --jobs 3 \
        --platform linux/amd64,linux/arm64,linux/arm/v7 \
        --pull-always \
        --manifest "$image" .

    podman manifest push "$image" "docker://docker.io/$image"
}

# goto this script dir
cd "$(dirname "$(readsymlink "$0")")"

# login docker hub
echo -n "docker hub password: "
read -rs pw < /dev/tty # tty due to git hook
echo "$pw" | podman login -u wollan --password-stdin docker.io

# push all >=v2 tags docker hub with updated baseimage
set -x
tags=($(git tag --sort creatordate | grep -v -e ^1 -e ^2))
latest_tag="${tags[-1]}"
for tag in "${tags[@]}"
do
    git checkout "$tag"
    build "wollan/devbox:$tag"

    if [[ "$tag" == "$latest_tag" ]]; then
        build "wollan/devbox:latest"
    fi
done
set +x

# upload readme for latest (last) tag
upload_readme "$pw"

# go back to where we were
git checkout "$BRANCH"

echo sucesss
