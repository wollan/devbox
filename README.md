# Development toolbox

Use as a development environment, debug tool or CI build base image, not in production.
Version 3 images are periodically updated with a freshly pulled ubuntu LTS base image.
Version 1 and 2 images are deprecated.

## Dockerfile links

- [3.0.0](https://bitbucket.org/wollan/devbox/src/3.0.0/Dockerfile) - 24.04 LTS
- [2.4.0](https://bitbucket.org/wollan/devbox/src/2.4.0/Dockerfile) - cron
- [2.3.0](https://bitbucket.org/wollan/devbox/src/2.3.0/Dockerfile) - xz
- [2.2.0](https://bitbucket.org/wollan/devbox/src/2.2.0/Dockerfile) - unzip
- [2.1.0](https://bitbucket.org/wollan/devbox/src/2.1.0/Dockerfile) - bzip2
- [2.0.0](https://bitbucket.org/wollan/devbox/src/2.0.0/Dockerfile) - multi-arch: amd64/arm64/armV7
- [1.11.0](https://bitbucket.org/wollan/devbox/src/1.11.0/x86/Dockerfile) - rsync, 22.04 LTS
- [1.10.0](https://bitbucket.org/wollan/devbox/src/1.10.0/x86/Dockerfile) - 20.04 LTS
- [1.9.0](https://bitbucket.org/wollan/devbox/src/1.9.0/x86/Dockerfile) - 18.04 LTS
- [1.8.2](https://bitbucket.org/wollan/devbox/src/1.8.2/x86/Dockerfile)
- [1.8.1](https://bitbucket.org/wollan/devbox/src/1.8.1/x86/Dockerfile) - file
- [1.8.0](https://bitbucket.org/wollan/devbox/src/1.8.0/x86/Dockerfile)
- [1.7.0](https://bitbucket.org/wollan/devbox/src/1.7.0/x86/Dockerfile) - nmap
- [1.6.1](https://bitbucket.org/wollan/devbox/src/1.6.1/x86/Dockerfile) - bash ignore-case
- [1.6.0](https://bitbucket.org/wollan/devbox/src/1.6.0/x86/Dockerfile) - ssh client
- [1.5.1](https://bitbucket.org/wollan/devbox/src/1.5.1/x86/Dockerfile) - net-tools, skip upgrade
- [1.5.0](https://bitbucket.org/wollan/devbox/src/1.5.0/x86/Dockerfile) 
- [1.4.3](https://bitbucket.org/wollan/devbox/src/1.4.3/x86/Dockerfile)
- [1.4.2](https://bitbucket.org/wollan/devbox/src/1.4.2/x86/Dockerfile) - x86, arm
- [1.4.1](https://bitbucket.org/wollan/devbox/src/1.4.1/Dockerfile) - apt upgrade
- [1.4.0](https://bitbucket.org/wollan/devbox/src/1.4.0/Dockerfile) - host
- [1.3.1](https://bitbucket.org/wollan/devbox/src/1.3.1/Dockerfile) - ca-certificates
- [1.3.0](https://bitbucket.org/wollan/devbox/src/1.3.0/Dockerfile) - sudo
- [1.2.0](https://bitbucket.org/wollan/devbox/src/1.2.0/Dockerfile) - docker, python
- [1.1.0](https://bitbucket.org/wollan/devbox/src/1.1.0/Dockerfile) - vim, less, jq
- [1.0.1](https://bitbucket.org/wollan/devbox/src/1.0.1/Dockerfile) - ping
- [1.0.0](https://bitbucket.org/wollan/devbox/src/1.0.0/Dockerfile) - curl, git

## Installed in latest version

- git
- curl
- docker
- ping
- vim
- less
- jq
- python 3
- sudo
- ca-certificates
- host
- net-tools (ifconfig..)
- ssh client
- nmap
- file
- rsync
- bzip2
- unzip
- xz
- cron

## Usage

Run bash with access to docker host daemon:

```
docker run --rm -itv /var/run/docker.sock:/var/run/docker.sock wollan/devbox

```

Run bash with docker CLI access to podman on host machine:

```
podman run --rm -it --security-opt label=disable -v /var/run/docker.sock:/var/run/docker.sock wollan/devbox
```

Run bash with access to current working dir:

```
docker run --rm -itv "$PWD":/w -w /w wollan/devbox
```
