FROM ubuntu:24.04

RUN apt-get update -qq \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yqq --no-install-recommends \
       tzdata \
       docker.io \
       curl \
       git \
       iputils-ping \
       vim \
       less \
       jq \
       python3 \
       python-is-python3 \
       sudo \
       ca-certificates \
       host \
       net-tools \
       openssh-client \
       nmap \
       file \
       rsync \
       bzip2 \
       unzip \
       xz-utils \
       cron \
 && rm -rf /var/lib/apt/lists/* \
 && printf '$include /etc/inputrc\nset completion-ignore-case On\n' > /root/.inputrc

WORKDIR /root
ENV TERM xterm-color
